
package final_project_2sem;

public class Item {
    int    id;
    String serialNo;
    String type;
    String brand;
    String model;
    double price;
    String status;
    String date;
    int    fk;
    
    public Item(int id, String serialNo, String type, String brand, String model, double price, String status, String date, int fk) {
        this.id       = id;
        this.serialNo = serialNo;
        this.type     = type;
        this.brand    = brand;
        this.model    = model;
        this.price    = price;
        this.status   = status;
        this.date     = date;
        this.fk       = fk;
    } 
    
    public Item(String serialNo, String type, String brand, String model, double price, String status, String date) {
        this.serialNo = serialNo;
        this.type     = type;
        this.brand    = brand;
        this.model    = model;
        this.price    = price;
        this.status   = status;
        this.date     = date;
    } 

    public int getId() {
        return id;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public String getType() {
        return type;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public double getPrice() {
        return price;
    }

    public String getStatus() {
        return status;
    }

    public String getDate() {
        return date;
    }

    public int getFk() {
        return fk;
    } 

    @Override
    public String toString() {
        return "Item{" + "id=" + id + ", serialNo=" + serialNo + ", type=" + type + ", brand=" + brand + ", model=" + model + ", price=" + price + ", status=" + status +", date=" + date + ", fk=" + fk + '}';
    }
}
