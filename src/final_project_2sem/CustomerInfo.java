/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package final_project_2sem;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class CustomerInfo extends javax.swing.JFrame {
    private static CustomerInfo c;
    private Connection conn;
    
    private CustomerInfo() {
        initComponents();
        conn = DBConnector.getConnection();
    }
    
    public static synchronized CustomerInfo getCustI() {
        if(c == null)
            c = new CustomerInfo();
        return c;
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        tfFirstName = new javax.swing.JTextField();
        tfAddress = new javax.swing.JTextField();
        tfPhone = new javax.swing.JTextField();
        tfEmail = new javax.swing.JTextField();
        bSaveCust = new javax.swing.JButton();
        bCancelCustInfo = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        tfLastName = new javax.swing.JTextField();
        custKey = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Customer Info");
        addWindowFocusListener(new java.awt.event.WindowFocusListener() {
            public void windowGainedFocus(java.awt.event.WindowEvent evt) {
                formWindowGainedFocus(evt);
            }
            public void windowLostFocus(java.awt.event.WindowEvent evt) {
            }
        });

        jLabel1.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        jLabel1.setText("First name");

        jLabel2.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        jLabel2.setText("Phone No");

        jLabel3.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        jLabel3.setText("Address");

        jLabel4.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        jLabel4.setText("Email");

        tfFirstName.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N

        tfAddress.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N

        tfPhone.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N

        tfEmail.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N

        bSaveCust.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        bSaveCust.setText("Save");
        bSaveCust.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bSaveCustActionPerformed(evt);
            }
        });

        bCancelCustInfo.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        bCancelCustInfo.setText("Cancel");
        bCancelCustInfo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bCancelCustInfoActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        jLabel5.setText("Last name");

        tfLastName.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(custKey, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(bSaveCust)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bCancelCustInfo))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(tfLastName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel1)
                                    .addComponent(jLabel5))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(34, 34, 34)
                                        .addComponent(tfFirstName, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(tfAddress, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(tfPhone, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(tfEmail, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))))))
                .addContainerGap(28, Short.MAX_VALUE))
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {tfAddress, tfEmail, tfFirstName, tfLastName, tfPhone});

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {bCancelCustInfo, bSaveCust});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(tfFirstName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(custKey, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(tfLastName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(tfAddress, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(tfPhone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(tfEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 31, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bSaveCust)
                    .addComponent(bCancelCustInfo))
                .addGap(20, 20, 20))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void bCancelCustInfoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bCancelCustInfoActionPerformed
        this.dispose();
    }//GEN-LAST:event_bCancelCustInfoActionPerformed

    private void bSaveCustActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bSaveCustActionPerformed
        String fName = tfFirstName.getText();
        String lName = tfLastName.getText();
        String address = tfAddress.getText();
        String phone = tfPhone.getText();
        String email = tfEmail.getText();
        
        if(checkEmptyField(tfFirstName) || checkEmptyField(tfLastName) || checkEmptyField(tfAddress) || checkEmptyField(tfPhone) || checkEmptyField(tfEmail)) {
            JOptionPane.showMessageDialog(this, "All fields must be filled in", "Warning", JOptionPane.WARNING_MESSAGE);
        }
        else if(checkPhone()) {
            if(custKey.getText().equals("")) {
                try {
                    //Update database
                    int custID = getID();
                    String sql = "INSERT INTO villa_watt_inventory.customers (CustomerID,FirstName,LastName,Address,PhoneNo,Email) VALUES("+custID+",?,?,?,?,?);";
                    PreparedStatement pstmt = conn.prepareStatement(sql);
                    pstmt.setString(1, fName);
                    pstmt.setString(2, lName);
                    pstmt.setString(3, address);
                    pstmt.setString(4, phone);
                    pstmt.setString(5, email);
                    pstmt.execute();

                    //Update customersTable
                    Inventory.getCustomersTableModel().addRow(new Object[] {custID,fName,lName,address,phone,email});

                    //Update map with objects
                    Customer cust = new Customer(custID, fName, lName, address, phone, email);
                    Inventory.getCustomersMap().put(custID, cust);
                    
                } catch (SQLException ex1) {
                    Logger.getLogger(CustomerInfo.class.getName()).log(Level.SEVERE, null, ex1);
                }
                JOptionPane.showMessageDialog(this, "Customer created");
                this.dispose();                
            }
            else {
                try { 
                    int key = Integer.parseInt(custKey.getText());
                    Customer c = new Customer(key, fName, lName, address, phone, email);
                    Customer oldC = Inventory.getCustomersMap().get(key);
                    if(!(oldC.getFName().equals(c.getFName()) && oldC.getLName().equals(c.getLName()) && oldC.getAddress().equals(c.getAddress()) && oldC.getPhoneNo().equals(c.getPhoneNo()) && oldC.getEmail().equals(c.getEmail()))) {
                        //Update map with objects
                        Inventory.getCustomersMap().replace(key, oldC, c);
                        
                        //Update database if customer already exists
                        String sql = "UPDATE villa_watt_inventory.customers SET FirstName = ?,LastName = ?,Address = ?,PhoneNo = ?,Email = ? WHERE CustomerID = ?;";
                        PreparedStatement pstmt = conn.prepareStatement(sql);
                        pstmt.setString(1, fName);
                        pstmt.setString(2, lName);
                        pstmt.setString(3, address);
                        pstmt.setString(4, phone);
                        pstmt.setString(5, email);
                        pstmt.setString(6, custKey.getText());
                        pstmt.execute();

                        //Update customersTable
                        int row = Inventory.getCustomersTable().getSelectedRow();
                        //Replace row
                        Inventory.getCustomersTableModel().removeRow(row);
                        Inventory.getCustomersTableModel().insertRow(row, new Object[] {custKey.getText(),fName,lName,address,phone,email});
                    
                        JOptionPane.showMessageDialog(this, "Customer's information saved");
                    }
                    this.dispose();            

                } catch (SQLException ex) {                
                    Logger.getLogger(CustomerInfo.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }//GEN-LAST:event_bSaveCustActionPerformed
    
    private int getID() {
        int rows = Inventory.getCustomersTable().getRowCount();
        if(rows == 0)
            return 1;
        else {
            int c = Integer.parseInt(Inventory.getCustomersTable().getValueAt(rows-1, 0).toString());
            int custID = c+1; //previous ID + 1 (custom incrementation)
            return custID;
        }
    }
    
    private void formWindowGainedFocus(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowGainedFocus
        rootPane.setDefaultButton(bSaveCust);
    }//GEN-LAST:event_formWindowGainedFocus
    
    private boolean checkEmptyField(JTextField f) {
        return f.getText().equals("");
    }
    
    public void cleanCustFields() {
        tfFirstName.setText("");
        tfLastName.setText("");
        tfAddress.setText("");
        tfPhone.setText("");
        tfEmail.setText("");
    }
    
    private boolean checkPhone() {
        try {
            int i = Integer.parseInt(tfPhone.getText());
        }catch(Exception e) {
            tfPhone.setText("");
            JOptionPane.showMessageDialog(this, "Invalid phone nr.", "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        return true;
    }
    
    public JTextField getFirstName() {
        return tfFirstName;
    }
    
    public JTextField getLastName() {
        return tfLastName;
    }
    
    public JTextField getAddress() {
        return tfAddress;
    }
    
    public JTextField getPhone() {
        return tfPhone;
    }
    
    public JTextField getEmail() {
        return tfEmail;
    }

    public JLabel getCustKey() {
        return custKey;
    }
    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CustomerInfo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CustomerInfo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CustomerInfo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CustomerInfo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new CustomerInfo().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bCancelCustInfo;
    private javax.swing.JButton bSaveCust;
    private javax.swing.JLabel custKey;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JTextField tfAddress;
    private javax.swing.JTextField tfEmail;
    private javax.swing.JTextField tfFirstName;
    private javax.swing.JTextField tfLastName;
    private javax.swing.JTextField tfPhone;
    // End of variables declaration//GEN-END:variables
}
